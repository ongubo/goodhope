@extends('layouts.dashboard')
@section('content')
<section id="content">
	<div id="breadcrumbs-wrapper" class=" grey lighten-3">
		<div class="container">
			<div class="row">
				<div class="col s12 m12 l12">
					<h5 class="breadcrumbs-title">Dashboard</h5>
					<ol class="breadcrumb">
						<li>
							<a href="#">User Dashboard</a>
						</li>
						<li>
							<a href="{{url('user')}}">User</a>
						</li>
						<li>
							<a href="#">User Profile</a>
						</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div id="profile-page-header" class="card">
			<div class="card-image waves-effect waves-block waves-light">
				<img class="activator" src="{{asset('assets/images/user-profile-bg.jpg')}}" alt="{{$user->name}}">
			</div>
			<figure class="card-profile-image">
				<img src="{{asset('assets/images/avatar.jpg')}}" alt="{{$user->name}}" class="circle z-depth-2 responsive-img activator">
			</figure>
			<div class="card-content">
				<div class="row">
					<div class="col s3 offset-s2">
						<h4 class="card-title grey-text text-darken-4">{{$user->name}}</h4>
						<p class="medium-small grey-text">{{$user->role}}</p>
					</div>
					<div class="col s2 center-align">
						<h4 class="card-title grey-text text-darken-4">78</h4>
						<p class="medium-small grey-text">Articles written</p>
					</div>
					<div class="col s2 center-align">
						<h4 class="card-title grey-text text-darken-4">Ksh 37,600</h4>
						<p class="medium-small grey-text">Total Earnings</p>
					</div>
					<div class="col s2 center-align">
						<a class=" modal-trigger" href="#editprof"><h4 class="card-title black-text">Edit profile</h4>
							<p>Click here to edit</p>
						</a>
					</div>
				</div>
			</div>
			<div class="card-reveal">
				<p>
					<span class="card-title grey-text text-darken-4">{{$user->name}} <i class="mdi-navigation-close right">
						</i>
					</span>
					<span>
						<i class="mdi-action-perm-identity cyan-text text-darken-2">
					</i> {{$user->role}}</span>
				</p>
				@if (empty($user->about))
				<strong>Please add something about you !</strong>
				@else
				<p>{{$user->about}}</p>
				@endif
				
				<p>
					<i class="mdi-action-perm-phone-msg cyan-text text-darken-2"></i>
					{{$user->telephone}}
					@if (empty($user->telephone))
					<strong>Please add your telephone number!</strong>
					@else
					{{$user->telephone}}
					@endif
				</p>
				<p>
					<i class="mdi-communication-email cyan-text text-darken-2">
				</i>{{$user->email}}</p>
			</div>
		</div>
		<div id="profile-page-content" class="row">
			<div id="profile-page-sidebar" class="col s12 m4">
				<div class="card green">
					<div class="card-content white-text">
						<span class="card-title">About {{$user->name}}<br></span>
						@if (empty($user->about))
						<strong>Please add something about you !</strong>
						@else
						<p>{{$user->about}}</p>
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="editprof" class="modal" style="display: none; opacity: 1; top: 10%;">
	<div class="modal-content">
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
		Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
	</div>
	<div class="modal-footer">
		<a href="#" class="waves-effect waves-red btn-flat modal-action modal-close">Disagree</a>
		<a href="#" class="waves-effect waves-green btn-flat modal-action modal-close">Submit</a>
	</div>
</div>
</section>
@stop