@extends('layouts.master')
@section('content')
    <div class="row">
            <?php foreach ($articles as $article): ?>
               <div class="col s12 m8 offset-m1">
                   <a href="article/<?=$article->id?> "><h4><?= $article->title ?></h4> </a><br>
                   <p> <?= $article->content ?></p>
               </div>
            <?php endforeach ?>
        </div>
@stop