@extends('layouts.auth')
@section('content')
<div id="login-page" class="row" >
    <div class="col s12 m5 l4 offset-m3 offset-l4 z-depth-4 card-panel">
        <form class="login-form white" role="form" method="POST" action="{{url('auth/register')}}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="row">
                <div class="input-field col s12 center">
                    <!-- <img src="images/login-logo.png" alt="" class="circle responsive-img valign profile-image-login"> -->
                    <h4 class="center-align">Register a new account</h4>
                </div>
            </div>
            @if (count($errors) > 0)
            <div class="row red-text center-align" style="font-size:small">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error}}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="row margin">
                <div class="input-field col s12">
                    <i class="mdi-social-person-outline prefix"></i>
                    <input id="name" style="cursor: auto; position: relative; font-size: 15px; white-space: pre-wrap; " type="text" class="form-control" name="name" value="{{ old('name') }}">
                    <label for="name" class="center-align">Name (s)</label>
                </div>
            </div>
            <div class="row margin">
                <div class="input-field col s12">
                    <i class="mdi-social-person-outline prefix"></i>
                    <input id="email" style="cursor: auto; position: relative; font-size: 15px; white-space: pre-wrap; " type="email" class="form-control" name="email" value="{{ old('email') }}">
                    <label for="email" class="center-align">Email Address</label>
                </div>
            </div>
            <div class="row margin">
                <div class="input-field col s12">
                    <i class="mdi-action-lock-outline prefix"></i>
                    <input id="password" type="password" name="password">
                    <label for="password" class="">Password</label>
                </div>
            </div>
           <div class="row margin">
                <div class="input-field col s12">
                    <i class="mdi-action-lock-outline prefix"></i>
                    <input id="password" type="password" name="password_confirmation">
                    <label for="password" class="">Confirm Password</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12 m6 offset-m3 ">
                    <button type="submit" class="btn waves-effect waves-light col s12 green">Register</button>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s6 m6 l6">
                    <p class="margin medium-small "><a href="{{url('auth/login')}}" >I already have an account.</a></p>
                </div>
            </div>
        </form>
    </div>
</div>
@stop