@extends('layouts.auth')
@section('content')
<div id="login-page" class="row" style="margin-top:6%">
	<div class="col s12 m4 l4 offset-m4 offset-l4 z-depth-4 card-panel">
		<form  class="login-form white"  role="form" method="POST" action="{{url('auth/login')}}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<div class="row">
				<div class="input-field col s12 center">
					<!-- <img src="images/login-logo.png" alt="" class="circle responsive-img valign profile-image-login"> -->
					<h4 class="center-align">Login to your account</h4>
				</div>
			</div>
			@if (count($errors) > 0)
			<div class="row red-text center-align" style="font-size:small">
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error}}</li>
					@endforeach
				</ul>
			</div>
			@endif
			<div class="row margin">
				<div class="input-field col s12">
					<i class="mdi-social-person-outline prefix"></i>
					<input id="email" style="cursor: auto; position: relative; font-size: 15px; white-space: pre-wrap; " type="email" class="form-control" name="email" value="{{ old('email') }}">
					<label for="email" class="center-align">Email Address</label>
				</div>
			</div>
			<div class="row margin">
				<div class="input-field col s12">
					<i class="mdi-action-lock-outline prefix"></i>
					<input id="password" type="password" name="password">
					<label for="password" class="">Password</label>
				</div>
			</div>
			<div class="row">
				<div class="input-field col s12 m12 l12  login-text">
					<input type="checkbox" id="remember-me" name="remember">
					<label for="remember-me">Remember me</label>
				</div>
			</div>
			<div class="row">
				<div class="input-field col s12 m6 offset-m3 ">
					<button type="submit" class="btn waves-effect waves-light col s12 green">Login</button>
				</div>
			</div>
			<div class="row">
				<div class="input-field col s6 m6 l6">
					<p class="margin medium-small "><a href="{{url('auth/register')}}" >Register Now</a></p>
				</div>
				<div class="input-field col s6 m6 l6">
					<p class="margin right-align medium-small "><a href="#" >Forgot password ?</a></p>
				</div>
			</div>
		</form>
	</div>
</div>
@stop