@extends('layouts.dashboard')
@section('content')
<?php $count=1;?>
<section id="content">
<div id="breadcrumbs-wrapper" class=" grey lighten-3">
	<div class="container">
		<div class="row">
			<div class="col s12 m12 l12">
				<h5 class="breadcrumbs-title">Dashboard</h5>
				<ol class="breadcrumb">
					<li>
						<a href="#">User Dashboard</a>
					</li>
					<li>
						<a href="{{url('user')}}">Home</a>
					</li>
					<li>
						<a href="#">User stats</a>
					</li>
				</ol>
			</div>
		</div>
	</div>
</div>
	<div class="container">
		<div id="card-stats">
			<div class="row">
				<div class="col s12 m6 l3">
					<div class="card">
						<div class="card-content  green white-text">
							<p class="card-stats-title">
								<i class="mdi-social-group-add">
							</i> Approved Articles</p>
							<h3 class="card-stats-number">{{$approved}}</h3>
						</div>
						<div class="card-action  green darken-2">
							
						</div>
					</div>
				</div>
				<div class="col s12 m6 l3">
					<div class="card">
						<div class="card-content grey white-text">
							<p class="card-stats-title">
								<i class="mdi-editor-attach-money">
							</i>Pending articles</p>
							<h3 class="card-stats-number">{{$pending}}</h3>
						</div>
						<div class="card-action grey darken-2">
							
						</div>
					</div>
				</div>
				<div class="col s12 m6 l3">
					<div class="card">
						<div class="card-content red darken-2 white-text">
							<p class="card-stats-title">
								<i class="mdi-action-trending-up">
							</i>Rejected Articles</p>
							<h3 class="card-stats-number">{{$rejected}}</h3>
						</div>
						<div class="card-action red darken-4">
							
						</div>
					</div>
				</div>
				<div class="col s12 m6 l3">
					<div class="card">
						<div class="card-content green accent-2 black-text">
							<p class="card-stats-title">
								<i class="mdi-editor-insert-drive-file">
							</i>Earnings</p>
							<h3 class="card-stats-number">Ksh 1,806</h3>
						</div>
						<div class="card-action  green accent-4 darken-2">
							
						</div>
					</div>
				</div>
			</div>
		</div>
	
<!-- articles table here -->
<h4 class="header">Your Articles</h4>
<div class="divider"></div>
<div class="row">
	<div class="col s12 m12">
		<table class="striped">
			<thead>
				<tr>
					<th></th>
					<th width="50%">Title</th>
					<th width="15%">Status</th>
					<th width="15%">Cost</th>
					<th >Written On</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($articles as $article)
				<tr>
					<td>{{$count++}}</td>
					<td><a target="_blank" href="{{url('article/'.$article->id)}}">{{ $article->title }}</a></td>
					<td>
						@if ($article->status == 'Rejected')
						    <span style='position:relative !important' class="red badge">{{ $article->status }}</span>
						@elseif ($article->status == 'Approved')
						    <span style='position:relative !important' class="green badge">{{ $article->status }}</span>
						@else
						    <span style='position:relative !important' class="grey badge">{{ $article->status }}</span>
						@endif
					</td>
					<td>{{ $article->cost }}</td>
					<td>{{ $article->created_at }}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
</div>
</section>
@stop