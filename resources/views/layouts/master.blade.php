<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
        <title>Goodhope</title>
        <!-- CSS  -->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="{{ URL::asset('assets/css/materialize.min.css') }}" type="text/css" rel="stylesheet" media="screen,projection"/>
        <link href="{{ URL::asset('assets/css/style.css') }}" type="text/css" rel="stylesheet" media="screen,projection"/>
    </head>
    <body>
    <div class="navbar-fixed ">
        <nav>
            <div class="nav-wrapper green">
                <a href="#!" class="brand-logo">Goodhope</a>
                <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
                <ul class="right hide-on-med-and-down">
                    <li><a href="{{url('/')}}">Home</a></li>
                    <li><a href="{{url('auth/login')}}">Login</a></li>
                    <li><a href="{{url('auth/register')}}">Register</a></li>
                </ul>
                <ul class="side-nav" id="mobile-demo">
                    <li><a href="{{url('/')}}">Home</a></li>
                    <li><a href="{{url('auth/login')}}">Login</a></li>
                    <li><a href="{{url('auth/register')}}">Register</a></li>
                </ul>
            </div>
        </nav>
    </div>
        @yield('content')
        <footer class="page-footer green">
            <div class="container">
                <div class="row">
                    <div class="col l6 s12">
                        <h5 class="white-text">Company Bio</h5>
                        <p class="grey-text text-lighten-4">Range-rover market into gang sprawl smart-refrigerator drugs camera Kowloon neon BASE jump tattoo motion plastic. Boat realism youtube shanty town nodal point neon smart-tower. Saturation point systemic market post-geodesic crypto-tank-traps drugs. </p>
                    </div>
                    <div class="col l3 s12">
                        <h5 class="white-text">Settings</h5>
                        <ul>
                            <li><a class="white-text" href="#!">Link 1</a></li>
                            <li><a class="white-text" href="#!">Link 2</a></li>
                            <li><a class="white-text" href="#!">Link 3</a></li>
                            <li><a class="white-text" href="#!">Link 4</a></li>
                        </ul>
                    </div>
                    <div class="col l3 s12">
                        <h5 class="white-text">Connect</h5>
                        <ul>
                            <li><a class="white-text" href="#!">Link 1</a></li>
                            <li><a class="white-text" href="#!">Link 2</a></li>
                            <li><a class="white-text" href="#!">Link 3</a></li>
                            <li><a class="white-text" href="#!">Link 4</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer-copyright">
                <div class="container">
                    Made by <a class="white-text text-lighten-3" href="http://hirazi.co.ke">Hirazi Kenya</a>
                </div>
            </div>
        </footer>
        <!--  Scripts-->
        <script src="{{ URL::asset('assets/js/jquery.min.js') }}"></script>
        <script src="{{ URL::asset('assets/js/materialize.min.js') }}"></script>
        <script src="{{ URL::asset('assets/js/init.js') }}"></script>
    </body>