<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
		<title>Goodhope</title>
		<!-- CSS  -->
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<link href="{{ URL::asset('assets/css/materialize.min.css') }}" type="text/css" rel="stylesheet" media="screen,projection"/>
		<link href="{{ URL::asset('assets/css/style.css') }}" type="text/css" rel="stylesheet" media="screen,projection"/>
	</head>
	<body>
		<header id="header" class="page-topbar">
			<div class="navbar-fixed">
				<nav class="green">
					<div class="nav-wrapper">
						<ul class="left">
							<li>
								<h1 class="logo-wrapper">
								<!-- <a href="index.html" class="brand-logo darken-1"> -->
								<!-- <img src="images/materialize-logo.png" alt="materialize logo">
							</a> -->
							<!-- <span class="logo-text">Good Hope</span> -->
							</h1>
						</li>
					</ul>
					<!-- Logout logic here -->
				</div>
			</nav>
		</div>
	</header>
	<div id="main">
		<div class="wrapper">
			<aside id="left-sidebar-nav">
					<a href="#" data-activates="slide-out" class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only green">
						<i class="mdi-navigation-menu">
						</i>
					</a>
			</aside>
				@yield('content')
				<aside id="left-sidebar-nav">
					<ul id="slide-out" class="side-nav fixed leftside-navigation ps-container ps-active-y" style="width: 240px;">
						<li class="user-details green darken-2">
							<div class="row">
								<div class="col col s4 m4 l4">
									<img src="{{asset('assets/images/avatar.jpg')}}" alt="" class="circle responsive-img valign profile-image">
								</div>
								<div class="col col s8 m8 l8">
									<a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn" href="#" data-activates="profile-dropdown">{{$user->name}}<i class="mdi-navigation-arrow-drop-down right">
										</i>
									</a>
									<p class="user-roal">{{$user->role}}</p>
								</div>
							</div>
						</li>
						<li class="bold ">
							<a href="{{url('user')}}" class="waves-effect waves-green">
								<i class="mdi-action-dashboard">
							</i> Dashboard</a>
						</li>
						@if ($user->role == 'admin')
						    <li class="bold ">
								<a href="#" class="waves-effect waves-green">
									<i class="mdi-action-dashboard">
								</i> Accounts</a>
							</li>
						@else
							<li class="bold">
								<a href="{{url('user/'.$user->id)}}" class="waves-effect waves-green">
									<i class="mdi-action-account-circle">
									</i> Profile </a>
							</li>
						@endif
						<li class="bold ">
							<a href="{{url('auth/logout')}}" class="waves-effect waves-green">
								<i class="mdi-hardware-keyboard-tab">
							</i> Log Out</a>
						</li>
					</ul>
				</aside>
			</div>
		</div>
		<!--  Scripts-->
        <script src="{{ URL::asset('assets/js/jquery.min.js') }}"></script>
        <script src="{{ URL::asset('assets/js/materialize.min.js') }}"></script>
        <script src="{{ URL::asset('assets/js/init.js') }}"></script>
	</body>
</html>