<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Article;
require_once '/vendor/fzaninotto/Faker/src/autoload.php';


class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(UserTableSeeder::class);
        $this->call(ArticleTableSeeder::class);

        Model::reguard();
    }
}

class UserTableSeeder extends Seeder {
    public function run()
    {
        $faker = Faker\Factory::create();
        
        DB::table('users')->delete();

        User::create(['name' => $faker->name,
                    'email' => $faker->email,
                    'role' => 'admin',
            ]);

        for ($i=0; $i < 20; $i++) { 
            $name=$faker->name;
             User::create(['name' => $name,
                    'email' => $faker->email,
                    'about' => $faker->realText($maxNbChars = 200, $indexSize = 2),
                    'account_status' => $faker->randomElement($array = array ('Active','Pending','Banned')),
                    'location' => $faker->city,
                    'telephone' => $faker->phoneNumber,
                    'password'=>$name,
            ]);
        }
    }

}

class ArticleTableSeeder extends Seeder {
    public function run()
    {
        $users = User::all();
        $faker = Faker\Factory::create();
        DB::table('articles')->delete();

        foreach ($users as $user) {
            $articles_count=$faker->numberBetween($min = 3, $max = 10);
            for ($i=0; $i < $articles_count; $i++) { 
                 Article::create(['user_id' => $user->id,
                    'title' => $faker->sentence($nbWords = 6),
                    'status' => $faker->randomElement($array = array ('Approved','Pending','Rejected')),
                    'content'=> $faker->paragraph($nbSentences = 20),
                ]);
            }
        }
    }

}
